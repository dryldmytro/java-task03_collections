package com.dryl.shipwithdroids;

import java.util.Queue;
import java.util.Random;

public class Droid extends Ship implements Comparable<Droid>{
    int id;

    public Droid(int id) {
        this.id = id;
    }

    public static void setDroid(Queue<? super Droid> arr){
        Random random = new Random();
        int i = random.nextInt(100);
        arr.add(new Droid(i));
    }

    @Override
    public int compareTo(Droid o) {
        return id-o.id;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                '}';
    }
}
