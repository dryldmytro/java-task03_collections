package com.dryl.deque;

public class Main {

  public static void main(String[] args) {
    MyDeque myDeque = new MyDeque();
    myDeque.addLast(1);
    myDeque.addLast(10);
    myDeque.addLast(20);
    myDeque.remove(20);
    myDeque.getLast();
    myDeque.getFirst();
    myDeque.size();
  }
}
