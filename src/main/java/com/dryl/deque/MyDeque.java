package com.dryl.deque;

import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class MyDeque<E> implements Deque<E> {

  private Object[] objects;

  public MyDeque() {
    this.objects = new Object[]{};
  }

  @Override
  public void addFirst(E o) {
    offerFirst(o);
  }

  @Override
  public void addLast(E o) {
    offerLast(o);
  }

  @Override
  public boolean offerFirst(E e) {
    if (objects.length == 0) {
      objects = new Object[1];
      objects[0] = e;
    } else {
      Object[] copyObjects = new Object[objects.length + 1];
      System.arraycopy(objects, 0, copyObjects, 1, objects.length);
      copyObjects[0] = e;
      objects = copyObjects;
    }
    return true;
  }

  @Override
  public boolean offerLast(E e) {
    if (objects.length == 0) {
      objects = new Object[1];
      objects[0] = e;
    } else {
      Object[] copyObjects = new Object[objects.length + 1];
      System.arraycopy(objects, 0, copyObjects, 0, objects.length);
      copyObjects[copyObjects.length - 1] = e;
      objects = copyObjects;
    }
    return true;
  }

  @Override
  public E removeFirst() {
    if (objects.length == 0) {
      return null;
    } else {
      Object[] copyObjects = new Object[objects.length - 1];
      System.arraycopy(objects, 1, copyObjects, 0, objects.length - 1);
      objects = copyObjects;
      return (E) objects;
    }
  }

  @Override
  public E removeLast() {
    if (objects.length == 0) {
      return null;
    } else {
      Object[] copyObjects = new Object[objects.length - 1];
      System.arraycopy(objects, 0, copyObjects, 0, objects.length - 1);
      objects = copyObjects;
      return (E) objects;
    }
  }

  @Override
  public E pollFirst() {
    return removeFirst();
  }

  @Override
  public E pollLast() {
    return removeLast();
  }

  @Override
  public E getFirst() {
    return peekFirst();
  }

  @Override
  public E getLast() {
    return peekLast();
  }

  @Override
  public E peekFirst() {
    return (E) objects[0];
  }

  @Override
  public E peekLast() {
    return (E) objects[objects.length - 1];
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    removeFirst();
    return true;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    removeLast();
    return true;
  }

  @Override
  public boolean add(E e) {
    addLast(e);
    return true;
  }

  @Override
  public boolean offer(E e) {
    addLast(e);
    return true;
  }

  @Override
  public E remove() {
    return removeLast();
  }

  @Override
  public E poll() {
    return removeLast();
  }

  @Override
  public E element() {
    return getLast();
  }

  @Override
  public E peek() {
    return peekLast();
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    if (objects.length == 0) {
      objects = c.toArray();
    } else {
      Object[] copyObjects = c.toArray();
      objects = new Object[objects.length + copyObjects.length];
      System.arraycopy(copyObjects, objects.length - copyObjects.length,
          objects, 0, copyObjects.length);
    }

    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    objects = null;
    return true;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    addAll((Collection<? extends E>) c);
    return true;
  }

  @Override
  public void clear() {
    objects = null;
  }

  @Override
  public void push(E e) {
    add(e);
  }

  @Override
  public E pop() {
    return remove();
  }

  @Override
  public boolean remove(Object o) {
    if (contains(o)) {
      Object[] copyObjects = new Object[objects.length - 1];
      int index = -1;
      for (int i = 0; i < objects.length; i++) {
        if (objects[i].equals(o)) {
          index = i;
          break;
        }
      }
      if (index == objects.length - 1) {
        removeLast();
      } else {
        for (int i = 0; i < objects.length - 1; i++) {
            if (i < index) {
                copyObjects[i] = objects[i];
            } else {
                copyObjects[i] = objects[i + 1];
            }
        }
        objects = copyObjects;
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
      boolean[] isTrue = new boolean[c.size()];
      for (Object o:c) {
          int i = 0;
          isTrue[i] = contains(o);
          i++;
      }
      for (Boolean b: isTrue) {
          if (b== false){
              return false;
          }else return true;
      }
      return false;
  }

  @Override
  public boolean contains(Object o) {
    for (Object objects : objects) {
      if (objects.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int size() {
    return objects.length;
  }

  @Override
  public boolean isEmpty() {
      if (objects.length==0){
          return true;
      }else return false;
  }

  @Override
  public Iterator<E> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public Iterator<E> descendingIterator() {
    return null;
  }
}
